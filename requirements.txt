appdirs==1.4.4
asgiref==3.2.10
astroid==2.4.2
colorama==0.4.3
distlib==0.3.1
Django==3.1.1
filelock==3.0.12
gunicorn==20.0.4
importlib-metadata==1.7.0
isort==5.5.2
lazy-object-proxy==1.4.3
mccabe==0.6.1
middleware==1.2.3
pylint==2.6.0
pytz==2020.1
six==1.15.0
sqlparse==0.3.1
toml==0.10.1
typed-ast==1.4.1
virtualenv==20.0.31
whitenoise==5.2.0
wrapt==1.12.1
zipp==3.1.0
